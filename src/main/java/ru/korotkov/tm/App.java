package ru.korotkov.tm;

import ru.korotkov.tm.constant.TerminalConst;

import java.util.ResourceBundle;
import java.util.Scanner;


public class App {

    public static void main(String[] args) {
        displayWelcome();
        run(args);
    }

    public static void run(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String command;
        while (scanner.hasNextLine()) {
            command = scanner.nextLine();
            if (!process(command)) {
                break;
            }
        }

/*
        if (args == null || args.length < 1) {
            return;
        }
        displayArgument(args[0]);
*/
    }

    public static boolean process(String command) {
        if (command == null || command.isEmpty()) {
            return true;
        }

        if (TerminalConst.CMD_EXIT.equals(command)) {
            return false;
        }

        displayArgument(command);
        return true;
    }

    public static void displayArgument(final String arg) {
        ResourceBundle bundle = ResourceBundle.getBundle("MessagesBundle");
        switch (arg) {
            case TerminalConst.CMD_VERSION:
                System.out.println(bundle.getString("version"));
                break;
            case TerminalConst.CMD_ABOUT:
                System.out.println(bundle.getString("about"));
                break;
            case TerminalConst.CMD_HELP:
                System.out.println(bundle.getString("help"));
                break;
            default:
                System.out.println(String.format(bundle.getString("stub"), arg));
                break;
        }
    }

    public static void displayWelcome() {
        ResourceBundle bundle = ResourceBundle.getBundle("MessagesBundle");
        System.out.println(bundle.getString("welcome"));
    }

}